The size of bool is:1 byte
The size of char is:1 byte
The size of int is:4 byte
The size of arr is:40 byte
The size of the first element arr is pointing to:4 bytes
The size of double is:8 bytes

The address of the bool is:140737440276386
The address of the char is:140737440276387
The address of the int is:140737440276388
The address of the arr is:140737440276272
The address of the arr[1] is:140737440276276
The address of the arr[0] is:140737440276272
The address of the double is:140737440276392
Can you type the address of arr[2] and press enter >>140737440276280
YES! You guessed right!
